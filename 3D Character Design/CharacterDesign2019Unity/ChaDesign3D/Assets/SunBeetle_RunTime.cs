﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunBeetle_Runtime : MonoBehaviour
{

    public List<Transform> doorBlinds = new List<Transform>();
    public SunBeetle_Editor sunEditor;

    // Start is called before the first frame update
    void Awake()
    {
        sunEditor = gameObject.GetComponent<SunBeetle_Editor>();
        doorBlinds = sunEditor.SendMessage("GetDoor"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
