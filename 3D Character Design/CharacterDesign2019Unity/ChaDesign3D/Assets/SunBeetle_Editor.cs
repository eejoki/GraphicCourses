﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]

public class SunBeetle_Editor : MonoBehaviour
{

    public List<Transform> doorBlinds = new List<Transform>();

    // Start is called before the first frame update
    void Awake()
    {


       
    }

    // Update is called once per frame
    void Update()
    {
        if (doorBlinds.Count < 6)
        {
            for (int i = 0; i < 6; i++)
            {
                doorBlinds.Add(transform.Find("Object/Door0" + (i + 1)));
                Debug.Log("Object/Door0" + i);
            }
        }
        else if (doorBlinds.Count > 6)
        {
            doorBlinds.Clear();
        }
    }
    public List<Transform> GetDoor()
    {
        return doorBlinds;
    }
}
